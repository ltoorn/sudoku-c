if [ ! -d "./target/" ]
then
    mkdir target
fi

common_flags="-std=c99 -Wall"
main_input="src/main.c"

generator_input="src/code_generator.c"
generator_output="src/generated.h"

if [ $# == 0 ]
then
    echo -e "Options:\nr\t-\trelease build;\nd\t-\tdebug build;\nclean\t-\tclean out target."
	exit 1
fi

gcc -O2 $common_flags -pedantic-errors -o target/code_generator $generator_input

target/code_generator $generator_output

if [ $1 == "d" ]
then
    gcc -g $common_flags -pedantic-errors -o target/solver-debug $main_input
elif [ $1 == "r" ]
then
    clang -O2 $common_flags -pedantic -o target/solver $main_input
elif [ $1 == "clean" ]
then
    rm target/*
else
    echo "Ignoring unknown option: $1"
fi

