#include "solver.h"
#include "generated.h"

int is_valid(int sud[], int index, int guess) {
	int *prs = peers[index];
	
	for (int i = 0; i < PEERS; ++i) {
		if (sud[prs[i]] == guess) {
			return 0; // Found a peer with the same value: invalid guess.
		}
	}

	return 1;
}

int solve(int sud[]) {
	int index = -1;
	for (int i = 0; i < SUDOKU; ++i) {
		if (sud[i] == 0) {
			index = i;
			break;
		}
	}

	if (index == -1) {
		return 1; // There are no more open indices: we have solved the puzzle.
	}

	for (int guess = 1; guess < 10; ++guess) {
		if (is_valid(sud, index, guess)) {
			sud[index] = guess;	// Enter a valid guess.

			if (solve(sud)) { 	// Try to solve from new position.
				return 1;
			}

			sud[index] = 0;     // Back track.
		}
	}

	return 0; // The puzzle was not solvable from this starting position.
}

int euler_sum(void) {
	int sud[SUDOKU], line, index, sum, c;
	FILE *fp;
	char *path = "resources/p096_sudoku_extended.txt";

	line = index = sum = 0;

	/* p096_sudoku.txt has the following format: first a line with a title
	   of the form "Grid xx", followed by 9 lines of the Sudoku rows, where
	   the 9 numbers of the row are separated by spaces. Each line ends with
	   a newline character. */
	if ((fp = fopen(path, "r")) == NULL) {
		fprintf(stderr, "Failed to open file: %s.", path);
		return -1;
	}

	while ((c = getc(fp)) != EOF) {
		if (c == '\n') {
			++line;
		}

		if (line > 9) {
			if (!solve(sud)) {
				return 0;
			}

			line = index = 0;

			sum += (100 * sud[0]) + (10 * sud[1]) + sud[2];
		}

		if (line > 0 && isdigit(c)) {
			sud[index++] = c - '0';
		}
	}

	fclose(fp);

	if (!solve(sud)) {
		return 0;
	}

	sum += (100 * sud[0]) + (10 * sud[1]) + sud[2];

	return sum;
}

int main(void) {
	int sum = euler_sum();
	if (sum) {
		printf("Euler sum: %d\n", sum);
	} else {
		printf("euler_sum(): something went wrong.\n");
	}
	return 0;
}

