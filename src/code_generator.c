/* 
 MIT License
 
 Copyright © 2019 Lucas Christiaan van den Toorn

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the Software), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/
#include <stdio.h>

#include "solver.h"

typedef struct row_column_grid_indices {
	int row;
	int column;
	int grid;
} Row_Column_Grid_Indices;

// Each index has 8 (row) + 8 (column) + 4 (grid, minus overlap) peers
int peers_table[SUDOKU][PEERS];

void generate_peers_lookup_table(void) {
	Row_Column_Grid_Indices indices[SUDOKU];

	// Compute all row, column, and grid indices associated with each
	// position in the Sudoku.
	for (int i = 0; i < SUDOKU; ++i) {
		Row_Column_Grid_Indices ix;

		ix.row = i / 9;
		ix.column = i % 9;
		ix.grid = (ix.row / 3) + ((ix.column / 3) << 2);

		indices[i] = ix;
	}
	
	// Compute all peer indices associated with each position in 
	// the Sudoku.
	for (int i = 0; i < SUDOKU; ++i) {
		int irow = indices[i].row;
		int iclm = indices[i].column;
		int igrd = indices[i].grid;

		int *prs = peers_table[i];

		// For any position p, x is a peer of p if x != p and both
		// x and p lie on the same row, column, or grid.
		for (int peer = 0, j = 0; j < SUDOKU && peer < PEERS; ++j) {
			if (j != i && (irow == indices[j].row || iclm == indices[j].column || igrd == indices[j].grid)) {
				prs[peer++] = j;
			}
		}
	}
}

int output(char *progname, char *path) {
	int exit_result = 0;
	FILE *file = fopen(path, "w");
	if (file) {
		fprintf(file, "// Sudoku solver -- %s -- generated code\n\n", progname);
		
		generate_peers_lookup_table();
	
		fprintf(file, "int peers[%d][%d] = {\n", SUDOKU, PEERS);

		for (int i = 0; i < SUDOKU; ++i) {
			fprintf(file, "\t{");
			for (int j = 0; j < PEERS; ++j) {
				fprintf(file, " %d", peers_table[i][j]);
				if (j < (PEERS-1)) {
					fprintf(file, ",");
				}
			}
			fprintf(file, " %s\n", (i < (SUDOKU-1) ? "}," : "}"));
		}
		fprintf(file, "};\n");

		fclose(file);
	} else {
		fprintf(stderr, "FATAL: Failed to open output file: %s\n", path);
		exit_result = 1;
	}

	return exit_result;
}

int main(int argc, char **argv) {
	int exit_result;
	if (argc == 2) {
		char *progname = argv[0];
		char *path = argv[1];
		exit_result = output(progname, path);
	} else {
		fprintf(stderr, "Please enter a valid path to an output directory.\n");
		exit_result = 1;
	}
	return exit_result;
}
